$(document).ready(function () {

    //yearNow
    $(".year-now").text(new Date().getFullYear());

    //form styler plugin
    $('.styler').styler();

    //slider init
    $(".rslides").responsiveSlides({
        timeout: 2000,
        nav: true,
        prevText: "",
        nextText: ""
    });

    //меняем стрелку при клике
    function toggleArrow(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".accordion-arrow")
            .toggleClass('arrow-bottom arrow-top');
    }

    $('#accordion').on('hidden.bs.collapse', toggleArrow);
    $('#accordion').on('shown.bs.collapse', toggleArrow);

    //меняем числа в поле ввода
    $(".more-arrow").on("click", function () {
        var currentValue = $(this).parent(".relative-block").siblings(".only-number-input").val();
        $(this).parent(".relative-block").siblings(".only-number-input").val(+currentValue + 1);
    });

    $(".down-arrow").on("click", function () {
        var currentValue = $(this).parent(".relative-block").siblings(".only-number-input").val();
        if (+currentValue === 0) {
            return false;
        }
        $(this).parent(".relative-block").siblings(".only-number-input").val(+currentValue - 1);
    });

    $('.spinner input').keypress(function (e) {
        if (!(e.which == 8 || e.which == 45 || (e.which > 47 && e.which < 58)))
            return false;
    });

    //инициализация спинера
    $(function(){
        $('#customize-spinner').spinner('changed',function(e, newVal, oldVal){
            $('#old-val').text(oldVal);
            $('#new-val').text(newVal);
        });
    })
});